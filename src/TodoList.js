import React, { Component } from "react";
import TodoItem from "./TodoItem";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo, index) => (
            <TodoItem
              key={index}
              title={todo.title}
              completed={todo.completed}
              handleClick={this.props.handleClick}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default TodoList;
