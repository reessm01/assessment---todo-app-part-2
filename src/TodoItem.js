import React, { Component } from "react";

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            title={this.props.title}
            onChange={this.props.handleClick}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            title={this.props.title}
            onClick={this.props.handleClick}
          />
        </div>
      </li>
    );
  }
}

export default TodoItem;
