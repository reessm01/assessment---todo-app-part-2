import React, { Component } from "react";
import { Route, Link, Switch } from "react-router-dom";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList";

class App extends Component {
  state = {
    todos: todosList,
    value: "",
    active: 0
  };

  componentDidMount() {
    this.setState({ active: this.countActive() });
  }

  countActive = () => {
    return this.state.todos.filter(task => !task.completed).length;
  };

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  handleSubmit = e => {
    if (e.key === "Enter") {
      e.preventDefault();
      const index = this.state.todos.length + 1;
      const newTodoList = this.state.todos;
      newTodoList.push({
        userId: 1,
        id: index,
        title: e.target.value,
        completed: false
      });
      e.target.value = "";
      this.setState({
        todos: newTodoList,
        value: "",
        active: this.countActive()
      });
    }
  };

  handleClick = e => {
    let tempList = this.state.todos;

    tempList.forEach((entry, index) => {
      if (entry.title === e.target.title) {
        switch (e.target.type) {
          case "checkbox":
            entry.completed = e.target.checked;
            break;
          case "submit":
            tempList.splice(index, 1);
            break;
          default:
        }
      }
    });
    this.setState({ todos: tempList, active: this.countActive() });
  };

  handleClearAll = () => {
    const incompleteList = this.state.todos;
    incompleteList.forEach((entry, index) => {
      if (entry.completed) incompleteList.splice(index, 1);
    });
    this.setState({ todos: incompleteList, active: this.countActive() });
  };

  render() {
    return (
      <Switch>
        <Route
          exact
          path="/"
          render={props => (
            <Index
              handleClearAll={this.handleClearAll}
              handleSubmit={this.handleSubmit}
              handleChange={this.handleChange}
              onClick={this.handleClearAll}
              todos={this.state.todos}
              handleClick={this.handleClick}
              homeClass={"selected"}
              activeClass={""}
              completedClass={""}
              activeCount={this.state.active}
            />
          )}
        />
        <Route
          path="/active"
          render={props => (
            <Index
              handleSubmit={this.handleSubmit}
              handleChange={this.handleChange}
              handleClearAll={this.handleClearAll}
              todos={this.state.todos.filter(task => !task.completed)}
              handleClick={this.handleClick}
              homeClass={""}
              activeClass={"selected"}
              completedClass={""}
              activeCount={this.state.active}
            />
          )}
        />

        <Route
          path="/completed"
          render={props => (
            <Index
              handleSubmit={this.handleSubmit}
              handleChange={this.handleChange}
              handleClearAll={this.handleClearAll}
              todos={this.state.todos.filter(task => task.completed)}
              handleClick={this.handleClick}
              homeClass={""}
              activeClass={""}
              completedClass={"selected"}
              activeCount={this.state.active}
            />
          )}
        />
      </Switch>
    );
  }
}

function Index(props) {
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          autoFocus
          onKeyDown={props.handleSubmit}
          onChange={props.handleChange}
          value={props.value}
        />
      </header>
      <TodoList todos={props.todos} handleClick={props.handleClick} />
      <Footer
        handleClearAll={props.handleClearAll}
        homeClass={props.homeClass}
        activeClass={props.activeClass}
        completedClass={props.completedClass}
        activeCount={props.activeCount}
      />
    </section>
  );
}

function Footer(props) {
  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>{props.activeCount}</strong> item(s) left
      </span>

      <ul className="filters">
        <li>
          <Link to="/" className={props.homeClass}>
            All
          </Link>
        </li>
        <li>
          <Link to="/active" className={props.activeClass}>
            Active
          </Link>
        </li>
        <li>
          <Link to="/completed" className={props.completedClass}>
            Completed
          </Link>
        </li>
      </ul>

      <button className="clear-completed" onClick={props.handleClearAll}>
        Clear completed
      </button>
    </footer>
  );
}

export default App;
